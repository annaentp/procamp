import com.sun.tools.javac.util.ArrayUtils;

import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

public class BigData {


    public static final String FIELD_DELIMITER = ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)";
    public static final String ROW_DELIMITER = "\n";

    public static void main(String[] agrs) {

        List<Row> rows = getRows();

        Map<String, List<Row>> groupedByCountry = rows.stream().collect(Collectors.groupingBy(el -> el.country));
        for (Map.Entry<String, List<Row>> stringListEntry : groupedByCountry.entrySet()) {
            Float sumPrice = 0F;
            int sumRating = 0;
            int sumFiveRating = 0;
            List<Row> countryRows = stringListEntry.getValue();
            for (Row row : countryRows) {
                sumPrice += row.price;
                sumRating += row.rating_count;
                sumFiveRating += row.rating_five_count;
            }
            String country = stringListEntry.getKey();
            String avgPrice = String.format("%.2f", sumPrice / countryRows.size());
            String fiveRateShare = String.format("%.2f", sumRating == 0 ? 0 :  (float) sumFiveRating * 100 / (float) sumRating);
            System.out.println("".equals(country) ? "Unknown" : country + " (" + countryRows.size() + " rows): Average price of product = " + avgPrice + ", Share of five-star products = " + fiveRateShare + "%");

        }

    }

    private static List<Row> getRows() {
        InputStream is = BigData.class.getClassLoader().getResourceAsStream("test-task_dataset_summer_products.csv");
        int i = 0;

        List<Row> rows = new ArrayList<>();

        try {
            Scanner inputStream = new Scanner(is);
            inputStream.useDelimiter(ROW_DELIMITER);
            while (inputStream.hasNext()) {
                i++;
                String data = inputStream.next();
                List<String> row = new ArrayList<>(Arrays.asList(data.split(FIELD_DELIMITER, -1)));
                if (i == 1556) {
                    int y = 9+8;
                }
                //FIXME A bit corrupted data
                while (row.size() < 43) {
                    String data2 = inputStream.next();
                    data += data2;
                    row = new ArrayList<>(Arrays.asList(data.split(FIELD_DELIMITER, -1)));
                }
                //FIXME end
//                System.out.println(i + " " + row);
                if (i == 1) {
                    continue;
                }
                rows.add(getRow(row));
            }
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rows;
    }

    private static Row getRow(List<String> rowList) {
        Row row = new Row();
        row.country = (rowList.get(29));
        row.price = ("".equals(rowList.get(2)) ? 0 : Float.parseFloat(rowList.get(2)));
        row.rating_count = ("".equals(rowList.get(8)) ? 0 : Integer.parseInt(rowList.get(8)));
        row.rating_five_count = ("".equals(rowList.get(9)) ? 0 : Integer.parseInt(rowList.get(9)));
        return row;
    }

    static class Row {
         String country; //origin_country - country of origin for the products
         float price; // - price of the products
         int rating_count; // - how many times the product has been rated by user/consumer
         int rating_five_count; // - how many times the product has been rated by user/consumer with five stars

    }
}
